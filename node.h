//node
//This is the node h file
//Justin Henn
//Assignment 2
//4/2/18
#ifndef NODE_H
#define NODE_H
#include <vector>
#include <iostream>
#include "token.h"

using namespace std;

 typedef struct node_t{

  string key;
  node_t *left, *mid_left, *mid_right, *right;
  vector<token> different_tokens;

}node_t;
  
#endif
