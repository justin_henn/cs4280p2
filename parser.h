//parser.h
//This is the h file for the parser
//Justin Henn
//Assignment 2
//4/2/18
#ifndef PARSER_H
#define PARSER_H

#include <string>
#include "node.h"

using namespace std;

node_t* parser(node_t* root, string filename);

#endif
