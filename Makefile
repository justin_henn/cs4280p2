CC	= g++		# The C compiler
CFLAGS	= -g		# Enable debugging by default
TARGET	= frontEnd
OBJS	= main.o parser.o scanner.o tree.o testTree.o

$(TARGET): $(OBJS)
	$(CC) -o $(TARGET) $(OBJS)

main.o:	main.cpp
	$(CC) $(CFLAGS) -c main.cpp

parser.o: parser.cpp
	$(CC) $(CFLAGS) -c parser.cpp

scanner.o: scanner.cpp
	$(CC) $(CFLAGS) -c scanner.cpp

testTree.o: testTree.cpp
	$(CC) $(CFLAGS) -c testTree.cpp

tree.o: tree.cpp
	$(CC) $(CFLAGS) -c tree.cpp
clean:
	/bin/rm -f *.o *~ *.sp18 *.txt *.out $(TARGET)
