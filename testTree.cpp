//traversal
//This is the traversal program for traversing and printing the tree
//Justin Henn
//Assignment 2
//4/2/18
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "node.h"
#include "testTree.h"

using namespace std;


/*OLD TREE FUNCTION*/


/*print tree in post order*/

/*void postOrder(node_t* node, int level, FILE* postfile) {

  if (node == NULL)
    return;

  string string_total = "";
  postOrder(node->left, level+1, postfile);

  postOrder(node->right, level+1, postfile);

  for (int i = (node->different_strings).size()-1; i >= 0; i--) {

    string_total += (node->different_strings)[i];
  }
  fprintf(postfile,"%*c%d:%-9s\n", level*2,' ',node->key,string_total.c_str());

} */

/*print tree in order*/

/*void inOrder(node_t* root, int level, FILE* infile) {

  if (root == NULL) 
    return;

    string string_total = "";
    inOrder(root->left, level+1, infile);

    for (int i = (root->different_strings).size()-1; i >= 0; i--) {

      string_total += (root->different_strings)[i];
    }
    fprintf(infile,"%*c%d:%-9s\n", level*2,' ',root->key,string_total.c_str());
    inOrder(root->right, level+1,infile);
}*/

/*print tree in pre order*/

void preOrder(node_t* node, int level, FILE* infile) {

  if (node == NULL)
    return;

  string string_total = "";

  for (int i = 0; i < (node->different_tokens).size(); i++) {

    string_total += " ";
    string_total += (node->different_tokens)[i].instance;
  }

  fprintf(infile,"%*c%s%-9s\n", level*2,' ',node->key.c_str(),string_total.c_str());

  preOrder(node->left, level+1, infile);
  preOrder(node->mid_left, level+1, infile);
  preOrder(node->mid_right, level+1, infile);
  preOrder(node->right, level+1, infile);
}


/*function to start parsing process*/

 void parseAll(node_t* node, int level, string filename) {

  string pre_filename = filename + ".out";

  FILE * prefile;
  if ((prefile = fopen(pre_filename.c_str(), "w")) == NULL) {

    printf("Cannot open file.\n");
    return;
  }

  preOrder(node, level, prefile);
  fclose(prefile);

}
